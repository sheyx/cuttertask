<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Сравнение сортировок заполнения</title>
</head>
<body>
<?

$cuttertTask = new CutterTask(99,365,array(14,35),10,8);
echo 'Тупая перебросом<br>';
$cuttertTask->calc_weekend_dump();
$cuttertTask->draw_weekend();
echo 'Тупая заполнением пустоты без сортировки<br>';
$cuttertTask->calc_weekend_dump_min_inline_wo_sort();
$cuttertTask->draw_weekend();
echo 'Тупая заполнением пустоты<br>';
$cuttertTask->calc_weekend_dump_min_inline();
$cuttertTask->draw_weekend();


class CutterTask
{

    public $userWeekend = array();
    private $weekendTiming = array();

    private $userCount = 10;
    private $weekendDuring = array(2, 14);
    private $dayAvalible = 100;
    private $lineCount = 2;
    private $departmentCount = 5;
    private $departmentColors = array();

    /**
     * CutterTask constructor.
     * @param int $userCount
     * @param int $dayAvalible
     * @param array $weekendDuring
     * @param int $lineCount
     * @param int $departmentCount
     */
    public function __construct(int $userCount = 10, int $dayAvalible = 100, array $weekendDuring = array(1, 14), int $lineCount = 2, int $departmentCount = 2)
    {
        $this->userCount = (int)$userCount;
        $this->dayAvalible = (int)$dayAvalible;
        $this->weekendDuring = (array)$weekendDuring;
        $this->lineCount = (int)$lineCount;
        $this->departmentCount = (int)$departmentCount;

        for ($i = 1; $i <= $this->departmentCount; $i++)
            $this->departmentColors[$i] = sprintf('#%02X%02X%02X', rand(0, 255), rand(0, 255), rand(0, 255));

        $this->gen_user_weekend();
    }

    /**
     * Генераци рандомного списка пользователей
     * @return array
     */
    private function gen_user_weekend()
    {
        for ($i = 1; $i <= $this->userCount; $i++)
            $this->userWeekend[] = array('id' => $i, 'during' => rand($this->weekendDuring[0], $this->weekendDuring[1]), 'department' => rand(1, $this->departmentCount));
        return $this->userWeekend;
    }

    /**
     * Тупое заполнение чередованием с сортировкой
     * @return array
     */
    public function calc_weekend_dump()
    {
        $this->gen_timing();
        $users = $this->userWeekend;
        usort($users, function ($arr1, $arr2) {
            return $arr1['during'] < $arr2['during'];
        });

        $lineLast = array();
        for ($i = 0; $i < $this->lineCount; $i++)
            $lineLast[$i] = 0;

        $line = 0;
        foreach ($users as $item) {
            $this->fill_line($item['id'], $item['department'], $line, $lineLast[$line], $item['during']);
            $lineLast[$line] += $item['during'];
            $line = ++$line < $this->lineCount  ? $line : 0;
        }
        return $this->weekendTiming;
    }

    /**
     * Генерация поля таймингов
     */
    private function gen_timing()
    {
        $this->weekendTiming = array();
        for ($i = 0; $i < $this->dayAvalible; $i++) {
            for ($j = 0; $j < $this->lineCount; $j++) {
                $this->weekendTiming[$j][] = array(0, 0);
            }
        }
    }

    /**
     * Заполнение стека отпуска
     * @param int $id
     * @param int $line
     * @param int $start
     * @param int $count
     */
    private function fill_line(int $id, int $department, int $line, int $start, int $count)
    {
        for ($i = $start; $i < $start + $count; $i++)
            $this->weekendTiming[$line][$i] = array($id, $department);
    }

    /**
     * Визуализация таблицы распределения
     */
    public function draw_weekend()
    {

        echo '<table border="1px">';
            foreach ($this->weekendTiming as $wLine){
                echo '<tr>';
                foreach ($wLine as $wD) {
                    echo '<td style="background-color:'.$this->departmentColors[$wD[1]].';">';
                    echo $wD[0];
                    echo '</td>';
                }
                echo '</tr>';
            }

        echo '</table>';

    }

    public function calc_weekend_dump_min_inline()
    {
        $this->gen_timing();
        $users = $this->userWeekend;
        usort($users, function ($arr1, $arr2) {
            return $arr1['during'] < $arr2['during'];
        });

        $lineLast = array();
        for ($i = 0; $i < $this->lineCount; $i++)
            $lineLast[$i] = 0;

        $line = 0;
        foreach ($users as $user) {
            $min = $this->dayAvalible;
            foreach ($lineLast as $k=>$v) {
                if ($v < $min){
                    $line = $k;
                    $min = $v;
                }
            }
            $this->fill_line($user['id'], $user['department'], $line, $lineLast[$line], $user['during']);
            $lineLast[$line] += $user['during'];
        }
        return $this->weekendTiming;
    }
    public function calc_weekend_dump_min_inline_wo_sort()
    {
        $this->gen_timing();
        $lineLast = array();
        for ($i = 0; $i < $this->lineCount; $i++)
            $lineLast[$i] = 0;

        $line = 0;
        foreach ($this->userWeekend as $user) {
            $min = $this->dayAvalible;
            foreach ($lineLast as $k=>$v) {
                if ($v < $min){
                    $line = $k;
                    $min = $v;
                }
            }
            $this->fill_line($user['id'], $user['department'], $line, $lineLast[$line], $user['during']);
            $lineLast[$line] += $user['during'];
        }
        return $this->weekendTiming;
    }

    private function calc_weekend_max_user_inwork()
    {

    }

}


function jcod($data)
{
    return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
}

?>

</body>
</html>
